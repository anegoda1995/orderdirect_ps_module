<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

/* Check security token */
if (isset($argc)) {
	for ($i = 0; $i < $argc; $i++) {
		$token = $argv[$i];
	}
}
if (
    (
        substr(Tools::encrypt('orderdirect/cron'), 0, 10) != $token ||
        substr(Tools::encrypt('orderdirect/cron'), 0, 10) != Tools::getValue('token')
    ) &&
    !Module::isInstalled('orderdirect')
) {
    die('Bad token');
}

if (!defined('_PS_MODE_DEMO_')) {
    define('_PS_MODE_DEMO_', false);
}

include(dirname(__FILE__).'/orderdirect.php');

$moduleObj = new OrderDirect();
$recievedProducts = $moduleObj->upgradeProducts();
var_dump($recievedProducts);