# Order Direct

## About

Module that allow you automatically (cron) update prices and add new products by Order Direct API (https://www.order-direct.nl). Moreover this module automatically will send new orders to Order Direct API.

## Requirements
This module working **ONLY WITH** **[composer package (library)](https://bitbucket.org/anegoda1995/orderdirect "composer package (library)")** that will be the layer between this module and Order Direct API.

## URLs
- https://bitbucket.org/anegoda1995/orderdirect_ps_module (module repository)
- https://bitbucket.org/anegoda1995/orderdirect (library repository)
- https://packagist.org/packages/anegoda1995/orderdirect (library on packagist)

## Installing
1. Install [library](https://packagist.org/packages/anegoda1995/orderdirect "library") by composer
2. Clone the **[module repository](https://bitbucket.org/anegoda1995/orderdirect_ps_module "module repository")** in *prestashop_folder/modules/orderdirectory* folder
3. Configure it in backoffice:
	- host;
	- username;
	- password;
	- session key (needed if some trouble with API and you want to set it manually, else - empty);
4. Set new row in your **cron** config for every minute. Be aware, don't change the token, only paths:
	- *crontab -e*
	- *php /home/user/www/project/modules/orderdirect/orderdirect-cron.php 41b56d1807 >> /home/user/www/project/error.log 2>&1*