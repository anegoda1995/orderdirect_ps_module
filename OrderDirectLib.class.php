<?php

use Anegoda1995\OrderDirect\OrderDirectCore;
use GuzzleHttp\Client;

class OrderDirectLib extends OrderDirectCore
{
    /**
     * @param string $method
     * @param array $params get params
     * @return stdClass
     * @throws Exception if error code returned or json decode failed
     */
    public function executeMethodGet($method, $params=array())
    {
        $params_str = implode('/', $params);
        if($params_str) $params_str .= '/';
        $res = $this->getClient()->get('http://' . $this->getConnection()->getHost() . '/odapi/rest/TMethods/'.$method.'/' . $this->getConnection()->getSessionKey() . "/{$params_str}json/");
        // $res->getStatusCode() == 200
        $responseJson = $res->getBody()->getContents();
        $responseObj = json_decode($responseJson);
        if(null == $responseObj && json_last_error()){
            throw new \Exception(json_last_error() . ': JSON decode error');
        }
        if (!empty($responseObj->error)) {
            throw new \Exception($responseObj->error->code . ': ' . $responseObj->error->message);
        }
        return $responseObj;
    }

    /**
     * @return Client
     */
    protected function getClient()
    {
        return new Client();
    }
}