<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use Anegoda1995\OrderDirect\OrderDirectConnection as ODConnection;
use Carbon\Carbon;

class OrderDirect extends Module
{
    /**
     * @var OrderDirectLib $odCore
     */
    protected $odCore;

    public function __construct()
    {
        $this->name = 'orderdirect';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7.4',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Order Direct');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('ORDERDIRECT_MODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (
            !parent::install() ||
            !$this->registerHook('actionValidateOrder')
        ) {
            $this->_errors[] = 'Unable to install module';

            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (
            !parent::uninstall() ||
            !$this->unregisterHook('actionValidateOrder')
        ) {
            return false;
        }
        return true;
    }

    /**
     * @return bool success
     */
    protected function ODInit()
    {
        // fallback to orderdirect/vendor path
        if(!class_exists('ODCore') && file_exists(__DIR__.'/vendor/autoload.php')){
            require_once "vendor/autoload.php";
        }
        require_once "OrderDirectLib.class.php";
        $this->odCore = new OrderDirectLib(new ODConnection(Configuration::get('ORDERDIRECT_HOST'), Configuration::get('ORDERDIRECT_USERNAME'), Configuration::get('ORDERDIRECT_PASSWORD'), Configuration::get('ORDERDIRECT_SESSION_KEY')));
        if ($this->odCore->getConnection()->isConnected()) {
            Configuration::updateValue('ORDERDIRECT_SESSION_KEY', $this->odCore->getConnection()->getSessionKey());
        }
        else{
            Configuration::updateValue('ORDERDIRECT_SESSION_KEY', '');
            return false;
        }
        return true;
    }

    public function hookActionValidateOrder(array $params)
    {
        // get user ($params['customer'])
        $customer = $params['customer'];
        $cart = $params['cart'];
        $products = $cart->getProducts();
        $order = $params['order'];

        if ($this->ODInit()) {

            // check via API is exist user (if no, create it)
            $relationId = $this->odCore->checkCustomerExistedAndCreateIfNot($customer->firstname . ' ' . $customer->lastname, $customer->email);

            // check every product on specific price for user
            $jsonArrayProducts = [];
            $todayDate = Carbon::now()->format('Ymd');
            foreach ($products as $product) {
                $dbProduct = new Product($product['id_product']);
                $productPrice = $this->odCore->getProductPriceForCustomer($relationId, $dbProduct->reference, $todayDate);
                $jsonArrayProducts[] = [
                    'quantity' => $product['cart_quantity'],
                    'price' => $productPrice,
                    'code' => $dbProduct->reference
                ];
            }
            
            // send order
            $orderSended = $this->odCore->createOrder($relationId, $customer->email, $todayDate, $jsonArrayProducts);
            
            PrestaShopLogger::addLog(
                'OrderDirect createOrder: '.$orderSended->data->preliminaryCustomerOrder->number, 
                1, 
                null, 
                'Order', 
                $order->id, 
                true, 
                null
            );            
        }
    }

    public function upgradeProducts($debug = false)
    {
        if ($this->ODInit()) {
            Configuration::updateValue('ORDERDIRECT_SESSION_KEY', $this->odCore->getConnection()->getSessionKey());
            $fromDateTime = Carbon::now()->subMinutes(2)->format('Ymdhis'); //year_month_date_hour_minute_second
            $toDateTime = Carbon::now()->format('Ymdhis');
            $recievedProducts = $this->odCore->getModifiedProducts($fromDateTime, $toDateTime);

            foreach ($recievedProducts as $recievedProduct) {
                $productsByCode = Product::searchByName((int)$this->context->language->id, $recievedProduct->product->code);

                $id_product = null;
                $update = false;
                if (!$productsByCode) {
                    $newProduct = $this->createProduct($recievedProduct);
                    $id_product = $newProduct->id;
                } else {
                    $existedProduct = $this->updateProduct($recievedProduct, new Product($productsByCode[0]['id_product']));
                    $id_product = $existedProduct->id;
                    $update = true;
                }
                unset($newProduct);
                unset($existedProduct);
                unset($productsByCode);

                PrestaShopLogger::addLog(
                    'OrderDirect product '.($update?'update':'create'), 
                    1, 
                    null, 
                    'Product', 
                    $id_product, 
                    true, 
                    null
                );        
            }
        }
        return $recievedProducts;
    }

    /**
     * @param array $recievedProduct ODProduct
     * @return Product
     */
    protected function createProduct($recievedProduct)
    {
        $newProduct = new Product();
        $newProduct->name = $recievedProduct->product->description;
        $newProduct->active = false;
        $newProduct->id_category = 0;
        $newProduct->reference = $recievedProduct->product->code;
        $newProduct->price = $recievedProduct->product->defaultSalesPrice;
        if(isset($recievedProduct->product->productGroup->number)){
            $newProduct->discount_group = $recievedProduct->product->productGroup->number;
        }
        $newProduct->add();
        return $newProduct;
    }

    /**
     * @param array $recievedProduct ODProduct
     * @param Product $product 
     * @return Product
     */
    protected function updateProduct($recievedProduct, $product)
    {
        $product->price = $recievedProduct->product->defaultSalesPrice;
        $product->save();
        return $product;
    }

    /**
     * For a given reference, returns the corresponding id
     *
     * @param string $reference
     * @return int id
     */
    public function getProductIdByReference($reference)
    {
        if (empty($reference)) {
            return 0;
        }

        $query = new DbQuery();
        $query->select('p.id_product');
        $query->from('product', 'p');
        $query->where('p.reference = \''.pSQL($reference).'\'');
        $query->orderBy('id_product DESC');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    /**
     * For a given reference, returns the corresponding id
     *
     * @param string $reference
     * @return int id
     */
    public function getAttributeIdByReference($reference)
    {
        if (empty($reference)) {
            return 0;
        }

        $query = new DbQuery();
        $query->select('pa.id_product_attribute');
        $query->from('product_attribute', 'pa');
        $query->where('pa.reference = \''.pSQL($reference).'\'');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    /**
     * @param string $sku
     * @return array ODProduct
     */
    public function getODProduct($sku)
    {
        if ($this->ODInit()) {
            try{
                $response = $this->odCore->executeMethodGet('product', [$sku]);
                return $response->data->product;
            }
            catch(\Exception $e){
                return false;
            }
        }
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('SAVE_CONFIGS')) {
            Configuration::updateValue('ORDERDIRECT_HOST', Tools::getValue('ORDERDIRECT_HOST'));
            Configuration::updateValue('ORDERDIRECT_USERNAME', Tools::getValue('ORDERDIRECT_USERNAME'));
            Configuration::updateValue('ORDERDIRECT_PASSWORD', Tools::getValue('ORDERDIRECT_PASSWORD'));
            Configuration::updateValue('ORDERDIRECT_SESSION_KEY', Tools::getValue('ORDERDIRECT_SESSION_KEY'));
            $output .= $this->displayConfirmation('Settings updated');
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => 'Settings',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => 'Host',
                    'name' => 'ORDERDIRECT_HOST',
                    'size' => 30,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => 'Username',
                    'name' => 'ORDERDIRECT_USERNAME',
                    'size' => 30,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => 'Password',
                    'name' => 'ORDERDIRECT_PASSWORD',
                    'size' => 30,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => 'Session key',
                    'name' => 'ORDERDIRECT_SESSION_KEY',
                    'size' => 50,
                    'required' => false
                ]
            ],
            'submit' => [
                'title' => 'Save',
                'name' => 'SAVE_CONFIGS',
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $fieldsForm[1]['form'] = [
            'legend' => [
                'title' => 'Check updating products',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'enabled' => false,
                    'size' => 30,
                    'name' => 'cron_url'
                ]
            ]
        ];



        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['ORDERDIRECT_MODULE_NAME'] = Configuration::get('ORDERDIRECT_MODULE_NAME');
        $helper->fields_value['ORDERDIRECT_HOST'] = Configuration::get('ORDERDIRECT_HOST');
        $helper->fields_value['ORDERDIRECT_USERNAME'] = Configuration::get('ORDERDIRECT_USERNAME');
        $helper->fields_value['ORDERDIRECT_PASSWORD'] = Configuration::get('ORDERDIRECT_PASSWORD');
        $helper->fields_value['ORDERDIRECT_SESSION_KEY'] = Configuration::get('ORDERDIRECT_SESSION_KEY');
        $helper->fields_value['cron_url'] = $this->cron_url = _PS_BASE_URL_ . _MODULE_DIR_ . $this->name . '/' . $this->name . '-cron.php?token=' . substr(Tools::encrypt($this->name . '/cron'), 0, 10);

        return $helper->generateForm($fieldsForm);
    }
}